import {BrowserWindow, app, globalShortcut} from 'electron';


let isFullScreen = false;

function createWindow () {
  const win = new BrowserWindow({
    center: true,
    resizable: true,
    webPreferences: {
      nativeWindowOpen: true
    }
  });

  win.webContents.openDevTools();
  win.maximize();
  win.loadURL('https://play.geforcenow.com/');
}

app.whenReady().then(() => {
  createWindow()

  app.on('activate', () => {
    if (BrowserWindow.getAllWindows().length === 0) {
      createWindow()
    }
  });

  globalShortcut.register("Super+F", () => {
    isFullScreen = BrowserWindow.getAllWindows()[0].isFullScreen();
    if (isFullScreen) {
      BrowserWindow.getAllWindows()[0].setFullScreen(false);
      isFullScreen = false;
    } else {
      BrowserWindow.getAllWindows()[0].setFullScreen(true);
      isFullScreen = true;
    }
  });
  globalShortcut.register('CommandOrControl+X', () => {
    new BrowserWindow({
      center: true,
      resizable: true,
      webPreferences: {
        nativeWindowOpen: true
      }
    })
        .loadURL('chrome://gpu/');


    new BrowserWindow({
      center: true,
      resizable: true
    })
        .loadURL('https://www.vsynctester.com/');
  })
})

app.on('browser-window-created', function(e, window) {
  window.setBackgroundColor('#1A1D1F');
  window.setMenu(null);

  window.on('leave-full-screen', function() {
    if (isFullScreen) {
      BrowserWindow.getAllWindows()[0].setFullScreen(true);
    }
  });

  window.on('page-title-updated', function(e, title) {
    if (title.includes('on GeForce NOW')) {
      window.setFullScreen(true);
      isFullScreen = true;
    }
  });
});

app.on('will-quit', () => {
  globalShortcut.unregisterAll();
});

app.on('window-all-closed', () => {
  if (process.platform !== 'darwin') {
    app.quit()
  }
})


app.commandLine.appendSwitch('enable-accelerated-mjpeg-decode');
app.commandLine.appendSwitch('enable-accelerated-video-decode');
app.commandLine.appendSwitch('ignore-gpu-blocklist');
app.commandLine.appendSwitch('enable-zero-copy');
app.commandLine.appendSwitch('enable-native-gpu-memory-buffers');
app.commandLine.appendSwitch('enable-gpu-rasterization');
app.commandLine.appendSwitch('disable-gpu-vsync');
app.commandLine.appendSwitch('disable-frame-rate-limit');
app.commandLine.appendSwitch('enable-oop-rasterization');
app.commandLine.appendSwitch('disable-gpu-driver-bug-workarounds');
app.commandLine.appendSwitch('enable-features', 'VaapiVideoDecoder');

